This is a stub for anti-patenting purposes and for anyone who might be interested.

## Intro

In this document I want to describe a functional model of a proto-AI which I think is implementable and testable using 
current technology.  
The core idea of this model is using human language as means of abstract thinking for an AI and 
constant self-questioning to determine actions.  
In a nutshell, in this model a bot constantly ASKS himself about what to do next and implements his own advices.  

## Usage  

The model is intended to be injected into a virtual world and undergo an evolution till it becomes sentient or close.

## Structure  

HIPAI (Houkime's introspective proto-ai) consists of the following modules:  

* Surroundings Analyzer, which describes current situation as text in a human language.  
	* Similar systems already exist to an extent.  
	* Due to the textual nature the "situation" here is not necessarily limited to just surrounding world. 
	It can also include 
		* past events 
		* assumptions about future
		* abstract constructs like known math theorems etc
		* inner state of the bot and his current activity
		* and whatever else which can be described by words
	* Different aspects can be analysed by different neural networks and just concatenated together.
* ASking System, which transforms the output of SA into a question.  
It can be as trivial as "What to do when X?" which is the main question for a bot, but can also probably handle making 
auxilary questions (for example to help SA with analisys conlusions).
* Answering system, which is basically a chatbot (probably one of already existing ones can suffice).  
It should answer to ASS with a human-language command (like "jump out of the window").
* Implementing System, which transforms human-language commands to the actions of a virtual creature 
	* This part is also semi-done with home assistants and similar networks.
* The loop, that invokes analysing-asking-implementation over and over ad infinum.
